DOWNLOAD_INFO = {
  "254.0.0-darwin_amd64": ("google-cloud-sdk-254.0.0-darwin-x86_64.tar.gz", "2fce2d0ab6c6d58658343c4d23e03f6ac84fda0e575401839e14ad7cd064d3fd"),
  "254.0.0-linux_amd64": ("google-cloud-sdk-254.0.0-linux-x86_64.tar.gz", "1bb4752645889a0a6a420787d7ceb9cbaa59ae2f8f42fd2338f8c2ffdafec8ca"),
}

def _download_info(version, host):
  key = "{}-{}".format(version, host)
  if key in DOWNLOAD_INFO:
    return DOWNLOAD_INFO[key]
  fail("Unknown host/version {}".format(key))

def _os_name(repository_ctx):
  os_name = repository_ctx.os.name.lower()
  if os_name.startswith("mac os"):
    return "darwin_amd64"
  elif os_name.startswith("linux"):
    return "linux_amd64"
  else:
    fail("Unsupported operating system: " + os_name)

def _download_sdk(repository_ctx):
  host = _os_name(repository_ctx)
  version = repository_ctx.attr.version
  filename, sha256 = _download_info(version, host)

  url = "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/{}".format(filename)
  repository_ctx.download_and_extract(
    url = [ url ],
    output = ".",
    stripPrefix = "google-cloud-sdk",
    sha256 = sha256,
  )

def _create_build_files(repository_ctx):
  repository_ctx.template("bin/BUILD.bazel", repository_ctx.attr._gcloud_sdk_bin_template)

def _google_cloud_sdk_repository_impl(repository_ctx):
  _download_sdk(repository_ctx)
  _create_build_files(repository_ctx)

google_cloud_sdk_repository = repository_rule(
  implementation = _google_cloud_sdk_repository_impl,
  attrs = {
    "version": attr.string(
      default = "254.0.0",
    ),
    "_gcloud_sdk_bin_template": attr.label(
      default = Label("//:gcloud_sdk_bin.BUILD.template"),
      allow_single_file = True,
    )
  },
)
