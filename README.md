# [ALPHA] Bazel rules for Google Cloud SDK

Alpha is here just because I'm targeting this for use in my projects and am not really considering backwards-compatibility. If you find it, use it at your own risk (or get in touch).

## Supported versions and platforms

All supported combinations of `version-platform` can be seen in [defs.bzl](./defs.bzl#L1)

## Usage

Initialize rules repository based on this repository. In most cases, you want to use latest commit hash.

```python
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")
git_repository(
    name = "rules_google_cloud",
    commit = "{{REPLACE_THIS_WITH_LATEST_COMMIT_HASH}}",
    remote = "https://gitlab.com/flipsid3/bazel/rules_google_cloud.git",
)
```

Initialize Google Cloud SDK repository (downloads SDK).

```python
load("@rules_google_cloud//:defs.bzl", "google_cloud_sdk_repository")
google_cloud_sdk_repository(
    name = "google_cloud_sdk",
)
```

`gcloud` binary is accessible as `@google_cloud_sdk//bin:gcloud`.

I'll probably be adding more rules but that's it for now.
